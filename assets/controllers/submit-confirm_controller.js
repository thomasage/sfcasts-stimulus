import {Controller} from 'stimulus'
import Swal from 'sweetalert2'
import {useDispatch} from "stimulus-use";

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static values = {
        confirmButtonText: String,
        icon: String,
        submitAsync: Boolean,
        title: String,
        text: String
    }

    connect() {
        useDispatch(this)
    }

    onSubmit(event) {
        event.preventDefault()
        Swal.fire({
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: this.confirmButtonTextValue || 'Yes',
            icon: this.iconValue || null,
            preConfirm: () => {
                return this.submitForm()
            },
            showCancelButton: true,
            showLoaderOnConfirm: true,
            text: this.textValue || null,
            title: this.titleValue || null
        })
    }

    async submitForm() {
        if (!this.submitAsyncValue) {
            this.element.submit()
            return
        }
        const response = await fetch(this.element.action, {
            method: this.element.method,
            body: new URLSearchParams(new FormData(this.element))
        })
        this.dispatch('async:submitted', {response})
    }
}
